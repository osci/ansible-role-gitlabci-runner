#!/usr/bin/env bash
PODMAN_RUN_ARGS+=('--sysctl=net.ipv4.ip_forward=1')
PODMAN_RUN_ARGS+=('--sysctl=net.ipv4.ping_group_range=0 0')
PODMAN_RUN_ARGS+=('--sysctl=net.ipv6.conf.all.forwarding=1')
PODMAN_RUN_ARGS+=('--device=/dev/fuse')
{% if with_podmaninpodman_systemd %}
# it does not give root privileges
# using only caps worked with F36 / podman 4.4.1
# starting with F37 / podman 4.4.2 this is no more sufficient
# using --privileged as a workaround for now
PODMAN_RUN_ARGS+=('--privileged')
#PODMAN_RUN_ARGS+=('--cap-add=sys_admin,net_admin,net_raw')
PODMAN_RUN_ARGS+=('--security-opt=label=disable')
PODMAN_RUN_ARGS+=('--security-opt=seccomp=unconfined')
PODMAN_RUN_ARGS+=('--systemd=always')
{% endif %}
PODMAN_RUN_ARGS+=('--uts=private')
PODMAN_RUN_ARGS+=('--env=container=podman')
if echo $CUSTOM_ENV_CI_RUNNER_TAGS | grep -q "\\<init\\>"; then
PODMAN_RUN_COMMAND=/sbin/init
fi

