# Ansible role for GitLab CI Runner deployment

## Introduction

[GitLab CI Runner](https://gitlab.com/gitlab-org/gitlab-runner) is the open
source project that is used to run your CI/CD jobs and send the results back
to GitLab.

This role installs and configure the runner using a dedicated user
(non-root). All you need is some space for local data (cache…, a few GBs
should do), and a registration token.

The configuration of podman can be made to support podman-in-podman with
systemd support (both in outer and inner container) but this comes with a few
security tradeoffs (a few extra capabilities, SELniux disabled, seccomp
disabled). With time we hope to reduce or even eliminate these tweaks. This
is useful is you with to run containers as lightweight VMs; we use it to run
Ansible molecule tests.

The (outer) container by default runs the `sleep` command but if you need
initd/systemd to run then you can ask for the init to be started by adding the
`init` tag to the gitlab default of step. This is not needed for
podman-in-podman with systemd to run fine.

## Usage

To read parameters documentation, use this command:

```
ansible-doc -t role gitlabci-runner

```
